/*
Copyright (c) 2017-2019, Muresan Vlad Mihail
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
*/
#ifndef _LIST_H
#define _LIST_H

#include <stdlib.h>
#include <stdio.h>

typedef struct list_t
{
  void* data;
  struct list_t* next;
  struct list_t* prev;
} list_t;

list_t* list_init();
void list_add(list_t* n, void* data);
void *std_list_get(list_t *l, void *data);
list_t* list_remove(list_t* n, void* data);
void list_destroy(list_t* n);
#endif
