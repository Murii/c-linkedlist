/*
Copyright (c) 2017-2019, Muresan Vlad Mihail
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
*/
#include "list.h"

#include <stdlib.h>
#include <stdio.h>

int main()
{

	list_t* l = list_init();
	list_add(l, "string");
	list_add(l, (int*)42);
	list_add(l, (int*)7991);

	int i = 0;

	while (l->next != NULL)
	{
		if (i == 0)
			printf("%s \n", (char*)l->data);
		else
			printf("%d \n", (int)l->data);
		i++;
		l = l->next;
	}

	list_destroy(l);
	return 0;
}

