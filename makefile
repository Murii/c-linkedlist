CC=gcc
EXE=out

OBJ=main.c list.c

default: all run

all: $(OBJ)
	$(CC) -std=c99 $^ -o $(EXE) -lm

run: 
	./$(EXE)

clean:
	-rm -f list.o main.o
	-rm -f $(EXE)
