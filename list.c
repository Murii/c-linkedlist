/*
Copyright (c) 2017-2019, Muresan Vlad Mihail
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
*/
#include "list.h"

list_t* list_init()
{
	list_t* l = malloc(sizeof(list_t));
	l->prev = NULL;
	l->next = NULL;
	l->data = NULL;
	return l;
}

void list_add(list_t* l, void* data)
{
	while (l->data != NULL)
	{
		if (l->next != NULL)
			l = l->next;
		else
			return;
	}

	l->data = data;
	l->next = malloc(sizeof(list_t));
	l->next->prev = l;
	l->next->next = NULL;
	l->next->data = NULL;
}

void *list_get(list_t *l, void *data)
{
    list_t *c = l;
    while (c->data != (data)) {
        if (c->data) {
            c = c->next;
        } else {
            return NULL;
        }
    }
    return c->data;
}

list_t* list_remove(list_t* l, void* data)
{
	list_t* d = l;

	// find node with matching data
	while (l->data != data)
	{
		if (l->next != NULL)
			l = l->next;
		else
			return d;
	}

	// remove node and adjust root
	if (l->prev != NULL)
	{
		if (l->next != NULL)
		{
			l->prev->next = l->next;
			l->next->prev = l->prev;
		}
		else
			l->prev->next = NULL;

	}
	else if (l->next != NULL)
	{
		// reset root node
		d = l->next;
		d->prev = NULL;
	}

	l->data = NULL;
	free(l);

	return d;
}

void list_destroy(list_t* l)
{
	if (l == NULL)
		return;

	// remove first node
	list_t* next = l->next;
	free(l);

	// remove the remaining nodes
	while (next != NULL)
	{
		l = next;
		next = next->next;
		free(l);
	}
}
